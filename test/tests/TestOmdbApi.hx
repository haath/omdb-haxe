package tests;

import omdb.OmdbClient;
import omdb.types.Rating;
import omdb.types.ResultType;
import omdb.types.parse.ParseRating;
import omdb.util.Util;
import utest.Assert;
import utest.Async;
import utest.ITest;


class TestOmdbApi implements ITest
{
    var testObject: OmdbClient;

    public function new()
    {
    }

    function setup()
    {
        testObject = new OmdbClient(Sys.getEnv('OMDB_API_KEY'), false);
    }

    @:timeout(1000)
    function testGetById(async: Async)
    {
        var b1: Async = async.branch();
        testObject.getById('tt0848228').map(r -> r.sure()).handle(movie ->
        {
            Assert.equals('The Avengers', movie.title);
            Assert.equals(4, movie.released.getMonth());

            for (rating in movie.ratings)
            {
                Assert.isTrue(rating.rating > 0 && rating.rating < 1);
                Assert.notNull(rating.source);
                Assert.notNull(rating.ratingStr);
            }

            b1.done();
        });

        var b2: Async = async.branch();
        testObject.getById('tt6718170').map(r -> r.sure()).handle(movie ->
        {
            Assert.equals('The Super Mario Bros. Movie', movie.title);
            Assert.equals(2023, movie.released.getFullYear());
            Assert.equals(3, movie.released.getMonth());
            Assert.equals(5, movie.released.getDate());

            for (rating in movie.ratings)
            {
                Assert.isTrue(rating.rating > 0 && rating.rating < 1);
                Assert.notNull(rating.source);
                Assert.notNull(rating.ratingStr);
            }

            b2.done();
        });
    }

    #if !python
    @:timeout(1000)
    function testGetByIdHttps(async: Async)
    {
        testObject = new OmdbClient(Sys.getEnv('OMDB_API_KEY'), true);
        testObject.getById('tt0848228').map(r -> r.sure()).handle(movie ->
        {
            Assert.equals('The Avengers', movie.title);
            Assert.equals(4, movie.released.getMonth());

            for (rating in movie.ratings)
            {
                Assert.isTrue(rating.rating > 0 && rating.rating < 1);
                Assert.notNull(rating.source);
                Assert.notNull(rating.ratingStr);
            }

            async.done();
        });
    }
    #end

    @:timeout(1000)
    function testGetByIdNotFound(async: Async)
    {
        testObject.getById('asdf').map(r -> r.sure()).handle(movie ->
        {
            Assert.isNull(movie);

            async.done();
        });
    }

    @:timeout(1000)
    function testGetByTitleNotFound(async: Async)
    {
        testObject.getByTitle('avengers', 1).map(r -> r.sure()).handle(movie ->
        {
            Assert.isNull(movie);

            async.done();
        });
    }

    @:timeout(2000)
    function testGetByTitle(async: Async)
    {
        // test without specifying year
        var b1: Async = async.branch();
        testObject.getByTitle('avengers').map(r -> r.sure()).handle(movie ->
        {
            Assert.equals('The Avengers', movie.title);
            Assert.equals(4, movie.released.getMonth());

            for (rating in movie.ratings)
            {
                Assert.isTrue(rating.rating > 0 && rating.rating < 1);
                Assert.notNull(rating.source);
                Assert.notNull(rating.ratingStr);
            }

            b1.done();
        });

        // test with year
        var b2: Async = async.branch();
        testObject.getByTitle('avengers', 2019).map(r -> r.sure()).handle(movie ->
        {
            Assert.equals('Avengers: Endgame', movie.title);
            Assert.equals(3, movie.released.getMonth());

            for (rating in movie.ratings)
            {
                Assert.isTrue(rating.rating > 0 && rating.rating < 1);
                Assert.notNull(rating.source);
                Assert.notNull(rating.ratingStr);
            }

            b2.done();
        });
    }

    @:timeout(1000)
    function testSearch(async: Async)
    {
        testObject.search('avengers').map(r -> r.sure()).handle(movies ->
        {
            Assert.isTrue(movies.length > 0);

            Assert.equals('The Avengers', movies[ 0 ].title);
            Assert.equals('tt0848228', movies[ 0 ].imdbID);
            Assert.equals(ResultType.Movie, movies[ 0 ].type);
            Assert.equals(2012, movies[ 0 ].year);

            async.done();
        });
    }

    @:timeout(1000)
    function testSearchOutOfPages(async: Async)
    {
        testObject.search('avengers', 100).map(r -> r.sure()).handle(movies ->
        {
            Assert.isNull(movies);

            async.done();
        });
    }

    @:timeout(1000)
    function testError(async: Async)
    {
        testObject = new OmdbClient('invalid_api_key');

        testObject.getById('tt0848228').handle(res ->
        {
            switch res
            {
                case Success(data):
                    Assert.fail();

                case Failure(failure):
                    Assert.equals(401, failure.code);
            }

            async.done();
        });
    }

    function testParseInvalidRating()
    {
        var method: ParseRating->Rating = @:privateAccess Util.parseRating;

        Assert.raises(() -> method(
            {
                Source: cast 'invalid source',
                Value: ''
            }));
    }
}
