<div align="center">

### haxelib-template

[![build status](https://gitlab.com/haath/omdb-haxe/badges/master/pipeline.svg)](https://gitlab.com/haath/omdb-haxe/pipelines)
[![test coverage](https://gitlab.com/haath/omdb-haxe/badges/master/coverage.svg)](https://gitlab.com/haath/omdb-haxe/-/jobs/artifacts/master/browse?job=test-neko)
[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://gitlab.com/haath/omdb-haxe/blob/master/LICENSEs)
[![haxelib version](https://badgen.net/haxelib/v/omdb)](https://lib.haxe.org/p/omdb)
[![haxelib downloads](https://badgen.net/haxelib/d/omdb)](https://lib.haxe.org/p/omdb)

</div>

---



Haxe client implementation of the [OMDB API](https://www.omdbapi.com/).

Currently, only the free public API is supported. If you're interested in the user-level API please make a PR.


## Installation

The library is available on [Haxelib](https://lib.haxe.org/p/omdb).

```
haxelib install omdb
```


## Usage

All the methods return tink_core [Promise](https://haxetink.github.io/tink_core/#/types/promise).

```haxe
var client: OmdbClient = new OmdbClient('api key');

client.getById('tt0848228').map(r -> r.sure()).handle(movie ->
{
    // movie.title, movie.year ...
});

client.getByTitle('avengers').map(r -> r.sure()).handle(movie ->
{
    // movie.title: The Avengers
    // movie.year: 2012
});

client.getByTitle('avengers', 2019).map(r -> r.sure()).handle(movie ->
{
    // movie.title: Avengers: Endgame
    // movie.year: 2019
});

client.search('the avengers').map(r -> r.sure()).handle(movies ->
{
    for (movie in movies)
    {
        // movie.title, movie.year ...
    }
});
```


## Versioning

This library will follow the `M.m.p` versioning scheme, where:

- `M`: the major number will follow the OMDB API version
- `m`: the minor number will be incremented when forward-breaking changes are made to the library's interface
- `p`: the patch number will be incremented when non-breaking changes are made to the library's interface
