package omdb.util;

import haxe.Json;
import omdb.types.*;
import omdb.types.parse.*;

using StringTools;
using StringTools;


class Util
{
    public static function parseMovie(jsonStr: String): Movie
    {
        var raw: ParseMovie = Json.parse(jsonStr);

        if (raw.Response != "True")
        {
            return null;
        }

        var movie: Movie =
        {
            title: raw.Title,
            year: Std.parseInt(raw.Year),
            rated: raw.Rated,
            released: parseDate(raw.Released),
            runtime: parseRuntime(raw.Runtime),
            genres: raw.Genre.split(', ').map(g -> cast(g, Genre)),
            directors: raw.Director.split(', '),
            writers: raw.Writer.split(', '),
            actors: raw.Actors.split(', '),
            plot: raw.Plot,
            language: raw.Language,
            country: raw.Country,
            awards: raw.Awards,
            poster: raw.Poster,
            ratings: raw.Ratings != null ? raw.Ratings.map(r -> parseRating(r)) : [ ],
            imdbVotes: raw.imdbVotes != null ? Std.parseInt(raw.imdbVotes.replace(',', '')) : 0,
            imdbID: raw.imdbID,
            type: raw.Type,
            dvd: raw.DVD != null ? parseDate(raw.DVD) : null,
            boxOffice: raw.BoxOffice != null ? Std.parseInt(raw.BoxOffice.replace('$', '').replace(',', '')) : 0,
            production: raw.Production,
            website: raw.Production,
        };
        return movie;
    }

    public static function parseSearchResults(jsonStr: String): Array<MovieSearchResult>
    {
        var rawResults:
            {
                Search: Array<ParseMovieSearch>,
                Response: String
            };
        rawResults = Json.parse(jsonStr);

        if (rawResults.Response != "True")
        {
            return null;
        }

        var movies: Array<MovieSearchResult> = [ ];

        for (raw in rawResults.Search)
        {
            movies.push(
                {
                    title: raw.Title,
                    year: Std.parseInt(raw.Year),
                    imdbID: raw.imdbID,
                    type: raw.Type,
                    poster: raw.Poster,
                });
        }

        return movies;
    }

    static inline function parseRuntime(runtimeStr: String): UInt
    {
        return Std.parseInt(runtimeStr.split(' ')[ 0 ]);
    }

    static inline function parseRating(ratingRaw: ParseRating): Rating
    {
        return
        {
            source: ratingRaw.Source,
            ratingStr: ratingRaw.Value,
            rating: switch ratingRaw.Source
            {
                case IMDB:
                    Std.parseFloat(ratingRaw.Value.split('/')[ 0 ]) / 10;

                case RottenTomatoes:
                    Std.parseFloat(ratingRaw.Value.split('&')[ 0 ]) / 100;

                case Metacritic:
                    Std.parseFloat(ratingRaw.Value.split('/')[ 0 ]) / 100;

                default:
                    throw 'unknown ratings source';
            }
        };
    }

    static inline function parseDate(dateStr: String): Date
    {
        if ((dateStr == null) || (dateStr == 'N/A'))
        {
            return null;
        }

        var parts: Array<String> = dateStr.split(' ');

        var monthNames: Array<String> = [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];

        return new Date(Std.parseInt(parts[ 2 ]), monthNames.indexOf(parts[ 1 ]), Std.parseInt(parts[ 0 ]), 0, 0, 0);
    }
}
