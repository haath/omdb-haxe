package omdb;

import haxe.Http;
import omdb.types.*;
import omdb.util.Util;
import tink.core.Error;
import tink.core.Outcome;
import tink.core.Promise;


class OmdbClient
{
    static inline final apiBaseUrl: String = 'http://www.omdbapi.com/';
    static inline final apiBaseSecureUrl: String = 'https://www.omdbapi.com/';

    final apiKey: String;
    final https: Bool;

    var userAgent: String;

    /**
     * Initialize a new client for connecting to the OMDB API.
     *
     * Note that the constructor only initializes the member variables of the object, and does not
     * perform any HTTP requests.
     *
     * @param apiKey the API key, which is required for authentication with the OMDB API (see https://www.omdbapi.com/apikey.aspx)
     * @param https whether to use the secure `https://` endpoint for connecting to the API instead of the insecure `http://`
     * @param userAgent the user-agent header to use when making requests;
     *                  for js targets this should probably be set to `null` since most browsers block CORS requests with a custom user-agent
     */
    public function new(apiKey: String, https: Bool = false, ?userAgent: String)
    {
        this.apiKey = apiKey;
        this.https = https;

        this.userAgent = userAgent;
    }

    /**
     * Get a title by its ID on IMDb.com.
     *
     * @param imdbId the ID of the title on IMDB.com (e.g tt1234567)
     * @param plot whether to fetch the short or the full plot summary for the title
     * @return the movie
     */
    public function getById(imdbId: String, plot: PlotLength = Full): Promise<Movie>
    {
        return
            httpGet([ 'i' => imdbId, 'plot' => cast(plot, String) ]).map(f -> switch f
            {
                case Success(data):
                    Outcome.Success(Util.parseMovie(data));
                case Failure(failure):
                    Outcome.Failure(failure);
            });
    }

    /**
     * Get a specific movie by its title, or a part of its title.
     *
     * @param title the title of the movie
     * @param year optional year of the movie; this can help narrow down the choice of only a partial title is inserted, or if multiple movies exist with the same title
     * @param plot whether to fetch the short or the full plot summary for the title
     * @return the movie
     */
    public function getByTitle(title: String, ?year: UInt, plot: PlotLength = Full): Promise<Movie>
    {
        var parameters: Map<String, String> = [ 't' => title, 'plot' => cast(plot, String) ];
        if (year != null && year > 0)
        {
            parameters.set('y', Std.string(year));
        }

        return
            httpGet(parameters).map(f -> switch f
            {
                case Success(data):
                    Outcome.Success(Util.parseMovie(data));
                case Failure(failure):
                    Outcome.Failure(failure);
            });
    }

    /**
     * Search for movies that match a given title.
     *
     * @param title the title to search for
     * @param page the page number to return; when the last page has been reached, `null` will be returned
     * @return the search results as a list with short info about movies
     */
    public function search(title: String, page: UInt = 1): Promise<Array<MovieSearchResult>>
    {
        return
            httpGet([ 's' => title, 'page' => Std.string(page) ]).map(f -> switch f
            {
                case Success(data):
                    Outcome.Success(Util.parseSearchResults(data));
                case Failure(failure):
                    Outcome.Failure(failure);
            });
    }

    function httpGet(parameters: Map<String, String>): Promise<String>
    {
        var req: Http = new Http(https ? apiBaseSecureUrl : apiBaseUrl);

        if (userAgent != null)
        {
            req.setHeader('User-Agent', userAgent);
        }

        req.addParameter('apikey', apiKey);
        req.addParameter('r', 'json');
        req.addParameter('v', '1');

        for (name => value in parameters)
        {
            req.addParameter(name, value);
        }

        var resp: PromiseTrigger<String> = Promise.trigger();

        req.onStatus = (status: Int) ->
        {
            switch status
            {
                case 200:
                // success

                default:
                    resp.trigger(Failure(new Error(status, 'http error')));
            };
        };
        req.onData = (data: String) ->
        {
            resp.trigger(Success(data));
        };
        req.request();

        return resp.asFuture();
    }
}
