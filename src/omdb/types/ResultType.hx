package omdb.types;


enum abstract ResultType(String)
{
    var Movie = "movie";
    var Series = "series";
    var Episode = "episode";
}
