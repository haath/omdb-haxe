package omdb.types.parse;

import omdb.types.RatingSource;


typedef ParseRating =
{
    var Source: RatingSource;
    var Value: String;
}
