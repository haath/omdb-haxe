package omdb.types.parse;


typedef ParseMovieSearch =
{
    var Title: String;
    var Year: String;
    var imdbID: String;
    var Type: ResultType;
    var Poster: String;
}
