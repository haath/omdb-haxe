package omdb.types;


typedef Movie =
{
    /** The title of the movie */
    var title: String;

    /** The release year of the movie */
    var year: UInt;

    /** The movie's certified rating */
    var rated: String;

    /** The release date of the movie */
    var released: Date;

    /** The movie's runtime in minutes */
    var runtime: UInt;

    /** List of genres applicable to the movie */
    var genres: Array<Genre>;

    /** List of names of directors of the movie */
    var directors: Array<String>;

    /** List of names of writers of the movie */
    var writers: Array<String>;

    /** List of names of actors of the movie */
    var actors: Array<String>;

    /** The movie's plot summary */
    var plot: String;

    /** The primary language of the movie */
    var language: String;

    /** The country where the movie was produced */
    var country: String;

    /** Descriptions of awards that the movie either received or was nominated for */
    var awards: String;

    /** The URL to the poster of the movie */
    var poster: String;

    /** List of ratings for the movie from various sources */
    var ratings: Array<Rating>;

    /** Number of votes the movie got on the IMDB */
    var imdbVotes: Int;

    /** The ID of the movie on imdb.com */
    var imdbID: String;

    /** The type of the movie object (either movie, series, or episode) */
    var type: ResultType;

    /** The date the movie was released on DVD */
    var dvd: Date;

    /** Revenue of the movie at the domestic box office in USD */
    var boxOffice: Int;

    /** Name of the production */
    var production: String;

    /** The URL to the website of the production */
    var website: String;
}
