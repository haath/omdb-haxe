package omdb.types;


typedef MovieSearchResult =
{
    /** The title of the movie */
    var title: String;

    /** The release year of the movie */
    var year: UInt;

    /** The ID of the movie on imdb.com */
    var imdbID: String;

    /** The type of the movie object (either movie, series, or episode) */
    var type: ResultType;

    /** The URL to the poster of the movie */
    var poster: String;
}
