package omdb.types;


enum abstract RatingSource(String)
{
    var IMDB = "Internet Movie Database";

    var RottenTomatoes = "Rotten Tomatoes";

    var Metacritic = "Metacritic";
}
