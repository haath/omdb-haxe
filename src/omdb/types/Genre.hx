package omdb.types;


enum abstract Genre(String)
{
    var Action = "Action";
    var Adventure = "Adventure";
    var Animation = "Animation";
    var Biography = "Biography";
    var Comedy = "Comedy";
    var Crime = "Crime";
    var Documentary = "Documentary";
    var Drama = "Drama";
    var Family = "Family";
    var Fantasy = "Fantasy";
    var FilmNoir = "Film-Noir";
    var GameShow = "Game-Show";
    var History = "History";
    var Horror = "Horror";
    var Music = "Music";
    var Musical = "Musical";
    var Mystery = "Mystery";
    var News = "News";
    var RealityTV = "Reality-TV";
    var Romance = "Romance";
    var SciFi = "Sci-Fi";
    var Sport = "Sport";
    var TalkShow = "Talk-Show";
    var Thriller = "Thriller";
    var War = "War";
    var Western = "Western";
}
