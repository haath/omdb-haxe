package omdb.types;


enum abstract PlotLength(String)
{
    var Short = "short";

    var Full = "full";
}
