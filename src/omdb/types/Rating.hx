package omdb.types;


typedef Rating =
{
    /** The source website of the rating */
    var source: RatingSource;

    /** The standardized rating as a number between `0.0` and `1.0` */
    var rating: Float;

    /** The rating in the as-is string format, which differs according to the source website */
    var ratingStr: String;
}
